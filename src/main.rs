use rand::Rng;
use std::io;

pub fn practice() {
    // Dao 1 chuoi str Slice
    // ex:1
    let str = "Ha Noi 2022";
    println!("String: {} ", str);
    println!("reverse: {} ", str.chars().rev().collect::<String>());
    // ex:2
    let mut str_rev = String::new();
    for char in str.chars() {
        //println!("chars {}", char);
        str_rev.insert(0, char);
    }
    println!("reverse2: {}", str_rev);
    // Guess loto

    println!("Enter the number:");
    let mut input = String::new();
    io::stdin().read_line(&mut input);
    input.pop();
    let input_parse = input.parse::<i32>().unwrap();
    let rand = rand::thread_rng().gen_range(0..100);
    println!("input {} rand {}", input, rand);
    if rand == input_parse {
        println!("Result: Your winner");
    } else {
        println!("Result: Loser");
    }
}

pub fn exercise1(org_arr: [i32; 8], sub_arr: [i32; 3]) {
    println!("arr {:?} {:?}", org_arr, sub_arr);
    let mut arr = Vec::new();
    let mut tamp = 0;
    for num1 in sub_arr.into_iter().enumerate() {
        let (i, x): (usize, i32) = num1;
        //println!("numb {} {}", i, x);
        for num2 in org_arr.into_iter().enumerate() {
            let (j, y): (usize, i32) = num2;
            if x == y && j >= tamp {
                println!("numb2 {} {}", j, y);
                tamp = j;
                arr.push(j);
                break;
            }
        }
    }

    if arr.len() == sub_arr.len() {
        println!("Is Sublist && correct ordinal");
    } else {
        println!("Don't Sublist || wrong ordinal");
    }
}
pub fn exercise2() {
    let str = "This is a regular paragraph with the default style of Normal. This is a regular paragraph with the default style of Normal. This is a regular paragraph with the default style of Normal. This is a regular paragraph with the default style of Normal. This is a regular paragraph with the default style of Normal.";
    println!("Input string need find ...");
    let mut input_str = String::new();
    io::stdin().read_line(&mut input_str);
    input_str.pop();
    let mut count = 0;
    for _char in str.split_whitespace() {
        if _char == input_str {
            count += 1;
        }
    }
    println!(
        "In gave sentence has {:?} match with {:?}",
        count, input_str
    );
}

fn main() {
    //practice();

    //exercise1([1, 2, 3, 5, 6, 10, 8, 11], [6, 8, 10]);
    exercise2();
}
